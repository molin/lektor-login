import os
import pytest
import shutil
import tempfile
import json
import requests

from requests import Session
from urllib.parse import urljoin

skip=pytest.mark.skip("not yet")

def login(client, name, password=None, url='/admin/root/edit', **kwargs):
    if not password:
        password = name
    return client.post('/auth/login',
            data = dict( username=name, password=password, url=url),
            **kwargs)

def logout(client, **kwargs):
    return client.get('/auth/logout', **kwargs)

# view cannot write anything
# blog can edit under blog
# test can edit under draft and under blog/draft, but not under blog
# admin has all rights

def write_data(path='/blog', title='test page', content='edited content'):
    return { 'data': {'_discoverable': None,
                '_hidden': None,
                '_slug': None,
                '_template': None,
                'title': title,
                'content': content},
            'path': path,
            'alt': '_primary' }

def new_file_data(path='/blog', id='foobar', title='new page'):
    return {'id': id, 'path': path, 'data': {'_model': 'page', 'title': title}}

@pytest.mark.minimal
def test_server_started(anonymous):
    rv = anonymous.get('/', timeout=0.1)
    assert rv.status_code == 200

@pytest.mark.server
def test_auth_buttons(anonymous):
    rv = anonymous.get('/', timeout=0.1)
    assert 'auth-button-div' in rv.text

@pytest.mark.server
def test_login_logout(anonymous):
    rv = login(anonymous, 'view')
    assert len(anonymous.cookies) > 0
    rv = login(anonymous, 'view', password='wrong')
    assert len(anonymous.cookies) == 0
    rv = login(anonymous, 'vie', password='view')
    assert len(anonymous.cookies) == 0
    rv = login(anonymous, 'view')
    assert len(anonymous.cookies) > 0
    rv = login(anonymous, 'view', url='/admin/root/edit')
    assert 'Loading' in rv.text
    rv = logout(anonymous)
    assert len(anonymous.cookies) == 0

@pytest.mark.server
def test_anonymous(anonymous):
    assert len(anonymous.cookies) == 0

@pytest.mark.server
def test_dash(anonymous, view):
    rv = anonymous.get('/admin/root/edit')
    assert 'Password' in rv.text
    rv = view.get('/admin/root/edit')
    assert 'Lektor Admin' in rv.text

@pytest.mark.server
def test_write(anonymous, view, draft, blog):

    data = write_data(path='/blog',content='edited content')
    rv = anonymous.put('/admin/api/rawrecord', json=data)
    assert ' "path": "/"' in rv.text
    rv = anonymous.get('/blog')
    assert 'original content' in rv.text

    rv = view.put('/admin/api/rawrecord', json=data)
    assert ' "path": "/"' in rv.text
    rv = view.get('/blog')
    assert 'original content' in rv.text

    rv = draft.put('/admin/api/rawrecord', json=data)
    assert ' "path": "/"' in rv.text
    rv = view.get('/blog')
    assert 'original content' in rv.text

    rv = blog.put('/admin/api/rawrecord', json=data)
    assert ' "path": "/blog"' in rv.text
    rv = view.get('/blog')
    assert 'edited content' in rv.text

    data = write_data(path='/blog',content='original content')
    rv = blog.put('/admin/api/rawrecord', json=data)
    assert ' "path": "/blog"' in rv.text
    rv = view.get('/blog')
    assert 'original content' in rv.text

@pytest.mark.server
def test_write_subpage(view, draft, blog):

    data = write_data(path='/blog',content='edited content')

    rv = draft.put('/admin/api/rawrecord', json=data)
    rv = draft.get('/blog')
    assert 'original content' in rv.text

    rv = blog.put('/admin/api/rawrecord', json=data)
    rv = blog.get('/blog')
    assert 'edited content' in rv.text

    data = write_data(path='/blog/draft',content='edited content')

    rv = view.get('/blog/draft/')
    assert 'draft content' in rv.text

    rv = draft.put('/admin/api/rawrecord', json=data)
    rv = view.get('/blog/draft/')
    assert 'edited content' in rv.text

    data = write_data(path='/blog/draft',title='test blog page',content='draft content')

    rv = blog.put('/admin/api/rawrecord', json=data)
    rv = view.get('/blog/draft/')
    assert 'draft content' in rv.text

@pytest.mark.server
def test_new_page(anonymous, draft, blog, admin):

    path = '/new-top-page'
    params = {'path': path}
    data = new_file_data(path='', id='new-top-page', title='new page')

    rv = anonymous.get('/admin/api/newrecord', params=params)
    assert '"label": "new-top-page"' not in rv.text
    rv = anonymous.post('/admin/api/newrecord',json=data)
    assert '"valid_id": false' in rv.text
    assert not json.loads(rv.text)["valid_id"]

    rv = draft.get('/admin/api/newrecord', params=params)
    assert '"label": "new-top-page"' not in rv.text
    rv = draft.post('/admin/api/newrecord',json=data)
    assert '"valid_id": false' in rv.text
    assert not json.loads(rv.text)["valid_id"]

    rv = admin.get('/admin/api/newrecord', params=params)
    print(rv.text)
    assert '"label": "new-top-page"' in rv.text
    rv = admin.post('/admin/api/newrecord',json=data)
    print(rv.text)
    assert '"path": "new-top-page"' in rv.text

    path = '/blog/blog-entry'
    params = {'path': path}
    data = new_file_data(path='/blog', id='blog-entry', title='first blog entry')

    rv = draft.get('/admin/api/newrecord', params=params)
    assert '"label": "new-top-page"' not in rv.text
    rv = draft.post('/admin/api/newrecord',json=data)
    assert not json.loads(rv.text)["valid_id"]

    rv = blog.get('/admin/api/newrecord', params=params)
    assert '"label": "blog-entry"' in rv.text
    rv = blog.post('/admin/api/newrecord',json=data)
    assert json.loads(rv.text)["valid_id"]

    path = '/blog/draft/blog-entry'
    params = {'path': path}
    data = new_file_data(path='/blog/draft', id='blog-entry', title='first blog entry')

    rv = draft.get('/admin/api/newrecord', params=params)
    assert '"label": "blog-entry"' in rv.text
    rv = draft.post('/admin/api/newrecord',json=data)
    assert json.loads(rv.text)["valid_id"]

@pytest.mark.server
def test_help_page(anonymous,blog):
    """
    check the help page is correctly registered
    """
    rv = blog.get('/auth/help')
    assert 'Compte et connexion' in rv.text

@pytest.mark.server
def test_blog_user(blog):
    """
    this user can edit anything under /blog
    """
    assert len(blog.cookies) > 0
    rv = blog.get('/admin/root/edit')
    print(rv.text)
    assert True
 
from flask import session as cookie
@pytest.mark.webui
def test_windex(webclient):
    rv = webclient.get('/')
    assert b'<body>' in rv.data
    print(rv.data)
    # buttons are missing
    # do not understand...
    # assert b'auth-button-div' in rv.data

@pytest.mark.webui
def test_wlogin_page(webclient):
    rv = webclient.get('/auth/login')
    assert b'Password' in rv.data

@pytest.mark.webui
def test_wlogin_logout(webclient):

    assert 'token' not in cookie
    rv = login(webclient,'draft', 'draft', 'toto')
    assert 'token' in cookie
    rv = logout(webclient)
    assert 'token' not in cookie

    rv = login(webclient,'user1', 'wrong', 'toto')
    assert 'token' not in cookie

@pytest.mark.webui
def test_wping(webclient):
    rv = webclient.get('/admin/api/ping')
    assert True

routes = [ ('/admin/api/previewinfo'   , 'GET'  ),
        ('/admin/api/pathinfo'      , 'GET'  ),
        ('/admin/api/recordinfo'    , 'GET'  ),
        ('/admin/api/rawrecord'     , 'GET'  ),
        ('/admin/api/newattachment' , 'GET'  ),
        ('/admin/api/newrecord'     , 'GET'  ),
        ('/admin/api/servers'       , 'GET'  ),
        ('/admin/api/rawrecord'     , 'PUT'  ),
        ('/admin/api/deleterecord'  , 'POST' ),
        ('/admin/api/newattachment' , 'POST' ),
        ('/admin/api/newrecord'     , 'POST' ),
        ('/admin/api/clean'         , 'POST' ),
        ('/admin/api/build'         , 'POST' )
        ]


@pytest.mark.webui
def test_wapi_routes(webclient):
    for (route,m) in routes:
        if m == 'GET':
            print(route, m)
            rv = webclient.get(route+'?path=%2Ftoto')
            assert rv is not None
        elif m == 'PUT':
            pass
            #rv = webclient.put(route, json={ 'data' : {} })
            #assert rv is not None
        elif m == 'POST':
            pass
            #rv = webclient.post(route,path='/toto')
            #assert rv is not None
    #rv = webclient.post('/admin/api/browsefs',q='/toto',path='/')
    #assert rv is not None
    rv = webclient.get('/admin/api/mathchurl?url_path=%s'%'%2Ftoto')
    assert rv is not None
    #rv = webclient.get('/admin/api/publish',server='')
    #assert rv is not None

@pytest.mark.webui
def test_wlogin_process(webclient):

    rv = login(webclient,'draft', 'draft', 'toto')
    assert b'You should be redirected automatically to target' in rv.data
    assert b'toto' in rv.data
    logout(webclient)

    rv = login(webclient,'draft', 'draft', '/admin/root/edit', follow_redirects=True)
    assert b'Lektor Admin' in rv.data

    logout(webclient)
    rv = login(webclient,'draft', 'draft', '/toto', follow_redirects=True)
    assert b'The requested URL was not found on the server' in rv.data

    logout(webclient)
    rv = login(webclient,'draft', 'draft', '/', follow_redirects=True)
    assert b'top page' in rv.data

@pytest.mark.webui
def test_wadmin_panel(webclient):
    """
    FIXME: non logged user should not see this page
    """
    logout(webclient)
    rv = webclient.get('/admin/root/edit')
    assert b'Lektor Admin' in rv.data

@pytest.mark.webui
def test_wwrite_file(webclient):
    rv = logout(webclient)
    data = write_data(path='')
    rv = webclient.put('/admin/api/rawrecord', json = data)
    # FIXME: this one should fail!
    print(rv.data)
    #assert b'"path": ""' in rv.data
    """
    returns b'{\n  "path": "/"\n}\n'
    """
    login(webclient, 'admin', 'admin', '/')
    rv = webclient.put('/admin/api/rawrecord', json = data)
    assert b'"path": "/"' in rv.data

    data = write_data(path='/blog')
    rv = webclient.put('/admin/api/rawrecord', json = data)
    assert b'"path": "/blog"' in rv.data

    logout(webclient)
    assert 'token' not in cookie
    rv = webclient.put('/admin/api/rawrecord', json = data)
    assert b'"path": "/blog"' in rv.data

@pytest.mark.webui
def test_wpath_preview(webclient):
    """
    must not be reachable
    """
    rv = webclient.get('/admin/api/recordinfo?path=')
    print(rv.data)
    assert b'can_have_attachments' in rv.data
    #assert b'Password' in rv.data

    rv = webclient.get('/admin/api/recordinfo?path=')
    print(rv.data)
    assert b'can_have_attachments' in rv.data
