# -*- coding: utf-8 -*-
import hashlib
import base64
import os
import datetime
import random
import string
from urllib.parse import urlparse, urljoin, unquote
import yaml
from markupsafe import Markup
from flask import Blueprint, \
                  request, \
                  redirect, \
                  current_app, \
                  session, \
                  url_for, \
                  render_template, \
                  flash, \
                  jsonify
from lektor.pluginsystem import Plugin, get_plugin
from lektor.admin.modules import serve, dash, api

authbp = Blueprint('auth', __name__,
                           url_prefix='/auth',
                           static_folder='static',
                           template_folder='templates'
                           )

class User(dict):
    @classmethod
    def from_file(cls, datafile):
        users = {}
        with open(datafile, 'r') as f:
            for u in yaml.safe_load_all(f):
                try:
                    u['password'] = base64.b64decode(u['password'])
                    users[u['id']] = cls(u)
                except KeyError as exc:
                    print("[WARNING: ILLEGAL USER DATA] ",exc)
        return users

def random_token():
    try:
        import secrets
        return secrets.token_hex(16)
    except ImportError:
        return os.urandom(16).hex()

def pwdhash(pwd):
    return hashlib.pbkdf2_hmac('sha256', bytes(pwd,'utf-8'), b'LektorLoginPlugin', 10000, 48)

from functools import wraps

@authbp.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.clear()
    flash('successfully logged out','info')
    return redirect('/')

@authbp.route('/error')
def error():
    return render_template('error.html')

@authbp.route('/help')
def help():
    return render_template('help_login.html')

def redirect_safe(target, default='/',**kwargs):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    if test_url.scheme in ('http', 'https') and \
                       ref_url.netloc == test_url.netloc:
        return redirect(target,**kwargs)
    return redirect(default,**kwargs)

def get_path_root():
    return '/'
def get_path_get():
    return request.args['path']
def get_path_post():
    return request.values['path']
def get_path_json():
    json = request.get_json(silent=True)
    return json.get('path','') if json else ''

#class ApiRequest:
#    error_value = {}
#    def __init__(self, request):
#        self.request = request
#    @property
#    def path(self):
#        return self.request.args.get('path')
#    def error(self):
#        return jsonify(self.error_value)
#    def authorize(self, user):
#        if user is None:
#            return self.error()
#        if auth == 'read' and self.read_all:
#            self.emit('access-api', route=path, user=user, request=request)
#            return
#        allowed = user.get(auth,[])
#        if any( [ path.startswith(prefix.rstrip('/')) for prefix in allowed ] ):
#            if self.verbose:
#                print('3. PATH ALLOWED')
#            self.emit('access-api', route=path, user=user, request=request)
#            return
#        return self.error()

api_requests = {
        # read access, requests used to feed dash
        ('/admin/api/previewinfo'   , 'GET') : (get_path_get  , 'read'  , { 'exists': False, 'url': None, 'is_hidden': True}),
        ('/admin/api/pathinfo'      , 'GET') : (get_path_get  , 'read'  , { 'segments': '[]'}),
        ('/admin/api/recordinfo'    , 'GET') : (get_path_get  , 'read'  , { 'exists': False, 'url': None, 'is_hidden': True}),
        ('/admin/api/browsefs'      , 'POST'): (get_path_post , 'read'  , { 'okay': False}),
        ('/admin/api/matchurl'      , 'GET') : (get_path_root , 'read'  , { 'exists': False, 'path': None, 'alt': None}),
        ('/admin/api/rawrecord'     , 'GET') : (get_path_get  , 'read'  , { 'exists': False, 'path': None, 'alt': None}),
        ('/admin/api/newattachment' , 'GET') : (get_path_get  , 'read'  , { 'can_upload': False, 'label': False}),
        ('/admin/api/servers'       , 'GET') : (get_path_root , 'read'  , { 'servers': '[]'}),
        # warning: must return an iterator
        ('/admin/api/publish'       , 'GET') : (get_path_root , 'read' ,  { 'msg ': ' "Error: cannot publish"'} ),
        # write access', 'action requests
        ('/admin/api/newrecord'     , 'GET') : (get_path_get  , 'write' , { 'label ': False, 'can_have_children ': False, 'implied_model': None, 'available_models': { 'Not allowed to add page here': {}} }),
        ('/admin/api/rawrecord'     , 'PUT') : (get_path_json , 'write' , { 'path': '/'}),
        ('/admin/api/deleterecord'  , 'POST'): (get_path_get  , 'write' , { 'okay': False}),
        ('/admin/api/newattachment' , 'POST'): (get_path_post , 'write' , { 'bad_upload ': True, 'path ': None, 'buckets ': ' []'}),
        ('/admin/api/newrecord'     , 'POST'): (get_path_json , 'write' , { 'valid_id': False, 'exists': False, 'path': None}),
        ('/admin/api/clean'         , 'POST'): (get_path_root , 'write' , { 'okay': False}),
        ('/admin/api/build'         , 'POST'): (get_path_root , 'write' , { 'okay': False}),
        }

def parse_api_request():
    """
    return path, default return value
    """
    try:
        getter, auth, error = api_requests.get((request.path, request.method))
        path = getter()
        path = unquote(path)
        if request.path == '/admin/api/publish':
            # publish must return an iterator
            return (path.rstrip('/'), auth, iter([error]))
        if request.path == '/admin/api/newrecord':
            print('NEWPAGE')
            print(request.get_json())
        return (path.rstrip('/'), auth, jsonify(error))
    except KeyError:
        # Should never be reached
        return ('/','write',jsonify(None))

class LoginPlugin(Plugin):
    name = 'lektor-login'
    description = u'Login support for lektor dev server.'
    #verbose = True
    verbose = False
    read_all = True
    usersdb = {}
    protected_routes = {}

    def emit(self, event, **kwargs):
        """ lektor bug, now fixed #859 """
        return self.env.plugin_controller.emit(self.id + "-" + event, **kwargs)

    def load_usersdb(self):
        config = self.get_config()
        dbfile = config.get('usersdb', None)
        confdir = os.path.dirname(self.config_filename)
        datafile = os.path.join(confdir, dbfile)
        self.usersdb = User.from_file(datafile)

    def on_setup_env(self, *args, **extra):

        config = self.get_config()

        secret_key = config.get('secret_key', '')
        assert secret_key != '', "set up secret_key in configs/login.ini"

        self.load_usersdb()
        self.verbose = config.get('verbose', False)
        self.read_all = config.get('read_all', True)

        @serve.bp.before_app_request
        #pylint: disable=unused-variable
        def setup_flask():
            """
            Flask app must have a secret key to access session cookie,
            but the server-spawn event is triggered before launching
            the flask app.
            As a workaround, we wait until first request to
            add the secret key.
            """
            app = current_app
            # mimic deprecated Flask before_app_first_request
            if app.config.get(__name__,False):
                return
            app.config.update(
                        SECRET_KEY = secret_key,
                        DEBUG = True,
                        REMEMBER_COOKIE_DURATION = datetime.timedelta(days=7)
                        )

            app.register_blueprint(authbp)

            admin_extra = get_plugin('admin-extra', self.env)
            url = url_for('auth.logout')
            svg = url_for('auth.static',filename='switch.svg')
            def is_logged():
                return session and session.get('token')
            admin_extra.add_button( url,
                    'logout',
                    Markup('<img src="%s" alt="logout">'%svg),
                    ignore=is_logged, index=0)

            # add link to help sections
            admin_extra.add_help_page(url_for('auth.help'), 'compte et connexion')
            app.config[__name__] = True

        @authbp.route('/login', methods=['GET', 'POST'])
        #pylint: disable=unused-variable
        def login():
            """
            if valid uid, check password and set random session token
            """
            try:
                if request.method == 'POST':
                    username = request.form['username']
                    password = request.form['password']
                    if username in self.usersdb:
                        user = self.usersdb[username]
                        if pwdhash(password) == user['password']:
                            token = random_token()
                            token = pwdhash(username+token)
                            session['user_id'] = user['id']
                            session['token'] = user['token'] = token
                            url = request.form['url']
                            return redirect_safe(url)
                    flash('wrong login/password','error')
                    session.clear()
                url = request.args.get('url','')
                return render_template('login.html', url=url)
            except (AssertionError, KeyError):
                flash('wrong login/password','error')
                session.clear()
                return render_template('login.html', url='/')

        @authbp.route('/reload_users')
        def reload():
            user = self.get_auth_user()
            if user and user.get('admin',False):
                self.load_usersdb()
            return redirect('/')

    def on_server_spawn(self, *args, **extra):
        """
        add before and after request process
        """

        @dash.bp.before_request
        #pylint: disable=unused-variable
        def before_request_dash():
            """
            intercepts requests /admin/...
            """
            if self.verbose:
                print("DASH INTERCEPT ", request.full_path)
            return self.validate_access_dash()

        @api.bp.before_request
        #pylint: disable=unused-variable
        def before_request_api():
            """
            intercepts requests /admin/api/...,
            except harmless ping requests

            see /lektor/admin/modules/api.py
            """
            if request.full_path.startswith('/admin/api/ping'):
                return
            if self.verbose:
                print("API INTERCEPT ", request)
            return self.validate_access_api()

        @api.bp.after_request
        #pylint: disable=unused-variable
        def after_request_api(response):
            if request.full_path.startswith('/admin/api/servers'):
                print("[Login] servers : %s = %s"%(response,response.data))
            elif request.full_path.startswith('/admin/api/servers'):
                print("[Login] servers : %s = %s"%(response,response.data))
        #    if self.verbose:
        #        print("RESPONSE ", response.data)
            return response

    def get_auth_user(self):
        try:
            user = self.usersdb.get(session['user_id'])
            assert session['token'] == user['token']
            return user
        except (AssertionError, KeyError) as e:
            if self.verbose:
                print('## [AUTH] REFUSED @assert token', e)
            return None

    def validate_access_api(self):
        """
        1. validates the session cookie
        2. checks the requested path is allowed
        """
        if self.verbose:
            print('## [AUTH] ASK FOR ACCESS')
        user = self.get_auth_user()
        path, auth, error = parse_api_request()
        if user is None:
            return error
        if self.verbose:
            print('0. VALID TOKEN')
        if auth == 'read' and self.read_all:
            self.emit('access-api', route=path, user=user, request=request)
            return
        allowed = user.get(auth,[])
        if any( [ path.startswith(prefix.rstrip('/')) for prefix in allowed ] ):
            if self.verbose:
                print('3. PATH ALLOWED')
            self.emit('access-api', route=path, user=user, request=request)
            return
        if self.verbose:
            print('3. PATH FORBIDDEN "%s"'%path)
        flash('You cannot edit this page.','error')
        return error

    def validate_access_dash(self):
        """
        Just give a warning if the logged user cannot edit this page
        """
        user = self.get_auth_user()
        if user is None:
            return redirect(url_for('auth.login', url=request.full_path))

    def access(self, **options):
        def decorator(f):
            @wraps(f)
            def decorated_function(*args, **kwargs):
                user = self.get_auth_user()
                if not user:
                    return redirect(url_for('auth.login', url=request.full_path))
                if 'level' in options and not user.get(options['level'],False):
                    flash("Vous n'avez pas le droit d'accéder à cette page.")
                    return redirect('/')
                return f(*args, **kwargs)
            return decorated_function
        return decorator


