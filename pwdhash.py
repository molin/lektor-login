import sys
import hashlib
import base64
def pwdhash(pwd):
    return hashlib.pbkdf2_hmac('sha256', bytes(pwd,'utf-8'), b'LektorLoginPlugin', 10000, 48)

if __name__ == '__main__' and len(sys.argv) > 1:
    pwd = pwdhash(sys.argv[1])
    b64 = base64.b64encode(pwd)
    print(b64)
