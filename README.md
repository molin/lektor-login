# Authentication

Small login plugin for Lektor.

The devel server asks users to authenticate,
and checks the session cookie at each request.

Further access management could be added.

## Configuration

Typical ``configs/login.ini`` configuration file:
```
secret_key = SECRET_KEY
usersdb = users.yaml
read_all = True # everyone logged in has read access to all pages
                # beware that the interface allows editions that will be
                # discarded
verbose = true  # show (lots of) debug information
```
where ``SECRET_KEY`` must be a true secret used by flask to
authenticate session cookies.

Then users can be registered in a ``configs/users.yaml`` file,
``read`` and ``write`` are lists of prefix.

```
blog:
  id: blog
  username: blog
  password: "<password hash in base64 encoding>"
  email: blog@dev.null
  read: ['/blog']  # read and edit only onder blog
  write: ['/blog']
  publish: true
```

the hash can be obtained using ``pwdhash`` utility
```
$ python3 pwdhash.py 'sesameouvretoi'
b'UOR4DHzhVFj3hd1aBLTiyu5l+SlphpW7MZ9GffTTbtd2TuZrVjyiJPbcWo54fVyS'
```

## About this plugin

### How lektor server works

A typical lektor edit involves the following requests

- to access the edition interface (dash)
   ```
   [GET] /admin/root:annee:m1-mic/edit?
   ```
- behind the scene, the dash javascript makes the following calls
  ```
  [GET] /admin/api/pathinfo?path=%2Fannee%2Fm1-mic
  [GET] /admin/api/rawrecord?path=%2Fannee%2Fm1-mic&alt=_primary
  [GET] /admin/api/recordinfo?path=%2Fannee%2Fm1-mic
  ```
- to save edits
  ```
  [PUT] /admin/api/rawrecord
  ```
  the edited content is then put as JSON
- to publish current build
  ```
  [GET] /admin/api/publish?server=null
  ```
  with the name of the deploy method

See the ``admin/server`` files in lektor for full list.

### This plugin

Each of these requests (except ``ping``) is intercepted by
this plugin using Flasks ``@before_request`` decorator,
and propagated only if the session cookie matches a logged user.

Edits requests are also compared to authorized paths for each user.

### Other changes

The plugin emits a signal on each authenticated access to the api.
```
def on_login_access_api(self, *args, **extra):
    # do something
    pass
```

## See also

Another plugin `lektor-commit` adds methods to commit and push changes
from the web interface.

## Future work

- [ ] use lektor-login or similar package
